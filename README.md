# ohwr-build-scripts

Shell scripts to automatise building of OHWR projects.

These scripts are known to work well on **Linux CentOS7** (RedHat kernel 3.10), which is the only
fully supported Linux distribution.

Furthermore, these instructions are also known to work (from the point of view of not producing any
errors during installation) in the following distributions:

* Debian 11 (Bullseye) with Linux kernel 5.10

However, there is no guarantee that things will properly work on these distributions, once
installed. Please report back any issues you find.

## Usage

The following environment variables affect the behaviour of the scripts.

* **$BUILD_DIR:** Points to the location where all the sources should be downloaded to and built. If
  not specified, a new directory will be created under `/tmp` using the `mktemp` program.
* **$KVERSION:** Kernel version to use. If not specified, it defaults to the output of `uname -r`.
* **$LINUX:** Path to Kernel sources/headers. If not specified, it defaults to
  `/lib/modules/$KVERSION/build`.
* **$SKIP_INSTALL:** If set to `true`, everything will be downloaded and built under `$BUILD_DIR`,
  but nothing will be installed.
* **$SKIP_BUILD:** If set to `true`, everything will be downloaded under `$BUILD_DIR`, but nothing
  will be built. This automatically implies `$SKIP_INSTALL=true`.
* **$SKIP_PATCH:** If set to `true`, no patches will be applied to the downloaded sources. This is
  useful in particular when using a custom `$BUILD_DIR` and the patches have already been applied
  during a previous run.
