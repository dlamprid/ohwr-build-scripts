#!/bin/sh

PROJECT=wrtd

. ./pre_script

bash $SCRIPT_DIR/mockturtle_build.sh

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://ohwr.org/project/wrtd.git $PROJECT
cd $PROJECT
git checkout -B release v1.1.0
export WRTD_DEP_TRTL=$BUILD_DIR/mock-turtle
[ $SKIP_BUILD ] || make -C software
[ $SKIP_INSTALL ] || sudo -E make -C software install
[ $SKIP_INSTALL ] || sudo -E sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/wrtd.conf'
[ $SKIP_INSTALL ] || sudo -E ldconfig

. $SCRIPT_DIR/post_script
