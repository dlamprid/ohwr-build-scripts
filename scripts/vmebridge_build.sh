#!/bin/sh

PROJECT=vmebridge

. ./pre_script

cd $BUILD_DIR

[ -d coht ] || git clone https://gitlab.cern.ch/dlamprid/coht.git
cd coht
git checkout varodek_develop
[ $SKIP_PATCH ] || [ -e $PATCH_DIR/$PROJECT.diff ] && patch -p1 < $PATCH_DIR/$PROJECT.diff
cd vmebridge-ng/driver
[ $SKIP_BUILD ] || make -C $LINUX M=`pwd` modules
[ $SKIP_INSTALL ] || sudo -E make -C $LINUX M=`pwd` modules_install
cd ../lib
[ $SKIP_BUILD ] || gcc -c -Wall -I../include -fPIC -DGIT_VERSION=\"none\" libvmebus.c libvmebus.h
[ $SKIP_BUILD ] || ar rv libvmebus.a libvmebus.o
cd ../vmeutils
CFLAGS="-Wall -I../include -DGIT_VERSION=\"none\" -L../lib -lvmebus"
[ $SKIP_BUILD ] || gcc $CFLAGS -o lsvme lsvme.c common.c
[ $SKIP_BUILD ] || gcc $CFLAGS -o vme-module vme-module.c module.c common.c
[ $SKIP_BUILD ] || gcc $CFLAGS -o vme-register vme-register.c register.c common.c
[ $SKIP_INSTALL ] || sudo -E install -d /usr/local/bin
[ $SKIP_INSTALL ] || sudo -E install -m 0755 lsvme /usr/local/bin
[ $SKIP_INSTALL ] || sudo -E install -m 0755 vme-module /usr/local/bin
[ $SKIP_INSTALL ] || sudo -E install -m 0755 vme-register /usr/local/bin

. $SCRIPT_DIR/post_script
