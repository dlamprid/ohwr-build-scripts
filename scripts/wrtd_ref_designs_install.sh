#!/bin/sh

PROJECT=wrtd-ref-designs

. ./pre_script

bash $SCRIPT_DIR/wrtd_build.sh

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://ohwr.org/project/wrtd-ref-designs.git $PROJECT
cd $PROJECT
git checkout -B release v2.0.0
[ $SKIP_INSTALL ] || sudo -E install -d /etc/udev/rules.d/
[ $SKIP_INSTALL ] || sudo -E install -m 0644 software/udev/rules.d/* /etc/udev/rules.d/
[ $SKIP_INSTALL ] || sudo -E sh -c 'getent group wrtd 2>&1 > /dev/null || groupadd wrtd'

BITSTREAM_URL="https://be-cem-edl.web.cern.ch/wrtd-reference-designs/v2.0.0/bitstreams/"
BITSTREAMS=$(curl -s $BITSTREAM_URL | grep 'tar.xz' | sed 's/.*<a href=".*">\(.*\)<\/a>.*/\1/')
mkdir -p $BUILD_DIR/bitstreams
for b in $BITSTREAMS; do
    curl $BITSTREAM_URL/$b | tar xJv -C $BUILD_DIR/bitstreams --wildcards '*.bin'
done

[ $SKIP_INSTALL ] || sudo -E install -d /lib/firmware
[ $SKIP_INSTALL ] || sudo -E install -m 0644 $BUILD_DIR/bitstreams/*.bin /lib/firmware/

. $SCRIPT_DIR/post_script
