#!/bin/sh

PROJECT=wrtd_ref_spec150t_adc

. ./pre_script

bash $SCRIPT_DIR/wrtd_ref_designs_install.sh
bash $SCRIPT_DIR/spec_carrier_build.sh
bash $SCRIPT_DIR/adc_lib_build.sh

. $SCRIPT_DIR/post_script
