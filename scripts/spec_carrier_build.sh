#!/bin/sh

PROJECT=spec

. ./pre_script

bash $SCRIPT_DIR/fmc_sw_build.sh
bash $SCRIPT_DIR/fpga_manager_build.sh
bash $SCRIPT_DIR/general_cores_build.sh
bash $SCRIPT_DIR/cheby_build.sh

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://ohwr.org/project/spec.git $PROJECT
cd $PROJECT
git checkout -B release v3.0.0
[ $SKIP_PATCH ] || [ -e $PATCH_DIR/$PROJECT.diff ] && patch -p1 < $PATCH_DIR/$PROJECT.diff
export CHEBY="python $BUILD_DIR/cheby/proto/cheby.py"
export FMC="$BUILD_DIR/fmc-sw"
export I2C="$BUILD_DIR/general-cores/software/i2c-ocores"
export SPI="$BUILD_DIR/general-cores/software/spi-ocores"
export CONFIG_FPGA_MGR_BACKPORT="y"
export FPGA_MGR="$BUILD_DIR/fpga-manager"
[ $SKIP_BUILD ] || make -C software
[ $SKIP_INSTALL ] || sudo -E make -C software install

. $SCRIPT_DIR/post_script
