#!/bin/sh

PROJECT=wrtd_ref_svec_tdc_x2

. ./pre_script

bash $SCRIPT_DIR/wrtd_ref_designs_install.sh
bash $SCRIPT_DIR/svec_carrier_build.sh

. $SCRIPT_DIR/post_script
