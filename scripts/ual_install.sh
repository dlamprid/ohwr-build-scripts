#!/bin/sh

PROJECT=ual

. ./pre_script

bash $SCRIPT_DIR/vmebridge_build.sh

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://gitlab.cern.ch/cohtdrivers/ual.git $PROJECT
cd $PROJECT
git checkout -B release v1.3.1
[ $SKIP_PATCH ] || [ -e $PATCH_DIR/$PROJECT.diff ] && patch -p1 < $PATCH_DIR/$PROJECT.diff
[ $SKIP_BUILD ] || make VMEBRIDGE=../../coht/vmebridge-ng CONFIG_VME=y
[ $SKIP_INSTALL ] || sudo -E make install
[ $SKIP_INSTALL ] || sudo -E sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/ual.conf'
[ $SKIP_INSTALL ] || sudo -E ldconfig
cd PyUAL
[ $SKIP_INSTALL ] || sudo -E python setup.py install

. $SCRIPT_DIR/post_script
