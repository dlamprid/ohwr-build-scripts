#!/bin/sh

PROJECT=fmc-adc-100m14b4cha

. ./pre_script

bash $SCRIPT_DIR/fmc_sw_build.sh
bash $SCRIPT_DIR/zio_build.sh
bash $SCRIPT_DIR/vmebridge_build.sh

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://ohwr.org/project/fmc-adc-100m14b4cha.git $PROJECT
cd $PROJECT
git checkout -B release v6.0.4
[ $SKIP_PATCH ] || [ -e $PATCH_DIR/$PROJECT.diff ] && patch -p1 < $PATCH_DIR/$PROJECT.diff
export ZIO=$BUILD_DIR/zio
export FMC=$BUILD_DIR/fmc-sw
export VMEBUS=$BUILD_DIR/coht/vmebridge-ng
[ $SKIP_BUILD ] || make -C software
[ $SKIP_INSTALL ] || sudo -E make -C software/kernel modules_install
[ $SKIP_INSTALL ] || sudo -E make -C software/tools install

BITSTREAM_URL="https://be-cem-edl.web.cern.ch/fmc-adc-100m14b4cha/v6.0.4/bitstreams/"
BITSTREAMS=$(curl -s $BITSTREAM_URL | grep 'tar.xz' | sed 's/.*<a href=".*">\(.*\)<\/a>.*/\1/')
mkdir -p $BUILD_DIR/bitstreams/$PROJECT
for b in $BITSTREAMS; do
    curl $BITSTREAM_URL/$b | tar xJv -C $BUILD_DIR/bitstreams/$PROJECT --wildcards '*.bin'
done

[ $SKIP_INSTALL ] || sudo -E install -d /lib/firmware
[ $SKIP_INSTALL ] || sudo -E install -m 0644 $BUILD_DIR/bitstreams/$PROJECT/*.bin /lib/firmware/

. $SCRIPT_DIR/post_script
