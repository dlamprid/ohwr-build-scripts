#!/bin/sh

PROJECT=mock-turtle

. ./pre_script

bash $SCRIPT_DIR/wbgen2_build.sh

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://ohwr.org/project/mock-turtle.git $PROJECT
cd $PROJECT
git checkout -B release v4.1.0
[ $SKIP_BUILD ] || WBGEN2=$BUILD_DIR/wishbone-gen/wbgen2 make -C software
[ $SKIP_INSTALL ] || WBGEN2=$BUILD_DIR/wishbone-gen/wbgen2 sudo -E make -C software install
[ $SKIP_INSTALL ] || sudo -E make -C software/lib/PyMockTurtle install
[ $SKIP_INSTALL ] || sudo -E sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/mockturtle.conf'
[ $SKIP_INSTALL ] || sudo -E ldconfig

. $SCRIPT_DIR/post_script
