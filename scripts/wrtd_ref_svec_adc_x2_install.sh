#!/bin/sh

PROJECT=wrtd_ref_svec_adc_x2

. ./pre_script

bash $SCRIPT_DIR/wrtd_ref_designs_install.sh
bash $SCRIPT_DIR/svec_carrier_build.sh
bash $SCRIPT_DIR/adc_lib_build.sh

. $SCRIPT_DIR/post_script
