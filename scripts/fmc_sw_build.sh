#!/bin/sh

PROJECT=fmc-sw

. ./pre_script

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://ohwr.org/project/fmc-sw.git $PROJECT
cd $PROJECT
git checkout -B release v1.1.5
[ $SKIP_PATCH ] || [ -e $PATCH_DIR/$PROJECT.diff ] && patch -p1 < $PATCH_DIR/$PROJECT.diff
[ $SKIP_BUILD ] || make
[ $SKIP_INSTALL ] || sudo -E make modules_install

. $SCRIPT_DIR/post_script
