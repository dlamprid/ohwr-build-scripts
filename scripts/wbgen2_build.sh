#!/bin/sh

PROJECT=wishbone-gen

. ./pre_script

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://ohwr.org/project/wishbone-gen.git $PROJECT
cd $PROJECT
git checkout master

. $SCRIPT_DIR/post_script
