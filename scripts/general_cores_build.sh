#!/bin/sh

PROJECT=general-cores

. ./pre_script

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://ohwr.org/project/general-cores.git $PROJECT
cd $PROJECT
git checkout -b tmp b84c76b
[ $SKIP_PATCH ] || [ -e $PATCH_DIR/$PROJECT.diff ] && patch -p1 < $PATCH_DIR/$PROJECT.diff
[ $SKIP_BUILD ] || make -C software/htvic
[ $SKIP_BUILD ] || make -C software/i2c-ocores
[ $SKIP_BUILD ] || make -C software/spi-ocores
[ $SKIP_INSTALL ] || sudo -E make -C software install

. $SCRIPT_DIR/post_script
