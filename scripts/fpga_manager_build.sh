#!/bin/sh

PROJECT=fpga-manager

. ./pre_script

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://gitlab.cern.ch/coht/fpga-manager.git $PROJECT
cd $PROJECT
git checkout master
[ $SKIP_BUILD ] || make
[ $SKIP_INSTALL ] || sudo -E make modules_install

. $SCRIPT_DIR/post_script
