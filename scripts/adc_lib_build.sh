#!/bin/sh

PROJECT=adc-lib

. ./pre_script

bash $SCRIPT_DIR/fmc_adc_100m_build.sh

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://ohwr.org/project/adc-lib.git $PROJECT
cd $PROJECT
git checkout -B release v4.0.3
[ $SKIP_PATCH ] || [ -e $PATCH_DIR/$PROJECT.diff ] && patch -p1 < $PATCH_DIR/$PROJECT.diff
export ZIO=$BUILD_DIR/zio
export FMCADC100M=$BUILD_DIR/fmc-adc-100m14b4cha/software
[ $SKIP_BUILD ] || make
[ $SKIP_INSTALL ] || sudo -E make install
[ $SKIP_INSTALL ] || sudo -E make -C PyAdcLib install

. $SCRIPT_DIR/post_script
