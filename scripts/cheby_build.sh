#!/bin/sh

PROJECT=cheby

. ./pre_script

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://gitlab.cern.ch/cohtdrivers/cheby.git $PROJECT
cd $PROJECT
git checkout -B release v1.5.0

. $SCRIPT_DIR/post_script
