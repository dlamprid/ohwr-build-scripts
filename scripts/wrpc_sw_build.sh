#!/bin/sh

PROJECT=wrpc-sw

. ./pre_script

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://ohwr.org/project/wrpc-sw.git $PROJECT
cd $PROJECT
git checkout -B release wrpc-v4.2
[ $SKIP_BUILD ] || make -C liblinux
[ $SKIP_BUILD ] || make -C liblinux/extest
[ $SKIP_BUILD ] || make -C tools wrpc-vuart wrpc-diags wr-streamers
[ $SKIP_INSTALL ] || sudo -E install -d /usr/local/bin
[ $SKIP_INSTALL ] || sudo -E install -m 0755 tools/wrpc-vuart   /usr/local/bin
[ $SKIP_INSTALL ] || sudo -E install -m 0755 tools/wrpc-diags   /usr/local/bin
[ $SKIP_INSTALL ] || sudo -E install -m 0755 tools/wr-streamers /usr/local/bin

. $SCRIPT_DIR/post_script
