#!/bin/sh

PROJECT=zio

. ./pre_script

cd $BUILD_DIR

[ -d $PROJECT ] || git clone https://ohwr.org/project/zio.git $PROJECT
cd $PROJECT
git checkout -B release v1.4.4
[ $SKIP_PATCH ] || [ -e $PATCH_DIR/$PROJECT.diff ] && patch -p1 < $PATCH_DIR/$PROJECT.diff
[ $SKIP_BUILD ] || make -C drivers/zio
[ $SKIP_INSTALL ] || sudo -E make -C drivers/zio modules_install

. $SCRIPT_DIR/post_script
